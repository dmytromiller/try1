const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'Try1';

// Create a new MongoClient
const client = new MongoClient(url, { useUnifiedTopology: true });

// Use connect method to connect to the Server
client.connect(function(err) {
    if(err) {
        console.log("Connection error: ", err);
    } else {
        console.log("Connected successfully to server");

        const db = client.db(dbName);


        client.close();
    }
});